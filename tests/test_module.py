# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class ProductionDailyEndTimeTestCase(ModuleTestCase):
    """Test Production Daily End Time module"""
    module = 'production_daily_end_time'


del ModuleTestCase
