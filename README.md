datalife_production_daily_end_time
==================================

The production_daily_end_time module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-production_daily_end_time/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-production_daily_end_time)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
