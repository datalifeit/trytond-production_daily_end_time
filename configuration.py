# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from datetime import datetime, date
from trytond.model import ModelSQL, fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.modules.company.model import CompanyValueMixin
from dateutil.tz import tz as dateutil_tz


class Configuration(metaclass=PoolMeta):
    __name__ = 'production.configuration'

    daily_end_time = fields.MultiValue(fields.Time('Daily end time'))

    def get_daily_end_date(self, date):
        Company = Pool().get('company.company')

        company = Transaction().context.get('company', None)
        if not company or self.daily_end_time is None:
            return None

        company = Company(company)
        tz = dateutil_tz.gettz(company.timezone or 'utc')
        tz_end_date = datetime.combine(date, self.daily_end_time)
        utc_end_date = tz_end_date.replace(tzinfo=tz).astimezone(
            dateutil_tz.tzutc()).replace(tzinfo=None)
        return utc_end_date

    def get_date_from_daily_end_time(self, date_, utc=False):
        # todo: implement time conversion if utc=True
        if self.daily_end_time is not None:
            hours = datetime.combine(date.min,
                self.daily_end_time) - datetime.min
            date_ = date_ - hours
        return date_.date()


class ConfigurationDailyEndTime(ModelSQL, CompanyValueMixin):
    '''Production configuration daily end time'''
    __name__ = 'production.configuration.daily_end_time'

    daily_end_time = fields.Time('Daily end time')
